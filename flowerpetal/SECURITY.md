# Security

The petal app runs on every server that is expected to receive requests from
the hub and create, modify or remove a resource.

Our main concerns are:

 * Did the request really come from the hub? We could be receiving a malicious
   submission. 

   * Use a shared secret (API Key). It's distributed in hash form to every petal 
     and stored in plain text on the hub. The key should be updated
     periodically.

   * The petal is configured to only respond to requests from a given host/IP
     address.

   * All requests come with a nonce. The petal queries the known hub address to
     ensure the nonce is valid.

 * Are we being tricked into doing something (as a non privileged user or as
   root) that is out of our scope.

   * *Do* run the petal as a non-privileged user and write simple bash scripts
     that should be run via sudo for code needing root privileges.

   * *Do* hard-code or use settings to define directories or things that should
     never change. For example: if you need to delete a home directory, don't
     write a sudo script that passed the full path with:

        rm -rf "$1"

     Instead just pass the username:
        
        [ -n "$1" ] && rm -rf /home/members/users/"$1"

   * In general, *don't* try to re-validate input. It's been validated by the hub.
     If we discover potentially dangerous input we should add more validation
     code to the hub. This approach provides simplicity, avoids duplication of
     code, and presents validation errors to the user where they are the most
     user friendly. If the hub is compromised, we are hosed anyway.

     However, sudo scripts should validate to avoid doing things out of scope. For
     example, don't insert variables directly into a path:

        rm "/etc/php/$1/pools.d/$2"

     Instead, test for expected values:

        version=

        if [ "$1" = "7.0" ]; then
          version=7.0
        elif [ "$2" = "7.1" ]; then
          version=7.1
        fi

        rm "/etc/php/$version/pools.d/$2"
