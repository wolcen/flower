from django.urls import path
from django.urls import include
from . import views

# Used to auto create coreapi schema 
urlpatterns = [
    path('', views.process_resource),
]
