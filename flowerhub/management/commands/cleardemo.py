from django.core.management.base import BaseCommand
from django.utils.translation import gettext as _
from flowerhub.models import Member
from flower.settings import FLOWER_DEMO

class Command(BaseCommand):
    help = _("Clear demo data by deleting all members.")

    def handle(self, *args, **kwargs):
        if not FLOWER_DEMO:
            print("Not in demo mode.")
            return
        members = Member.objects.all()
        for member in members:
            print("Deleting {0}".format(member))
            member.delete()
