from django.core.management.base import BaseCommand
from django.utils.translation import gettext as _
from flowerhub import models
import requests
from flowerhub.utils import get_random_password
from flowerhub import shared
from django.utils import timezone
from tabulate import tabulate
from django.db.models import F, Q

class Command(BaseCommand):
    help = _("List or Submit runners.")

    def add_arguments(self, parser):
        parser.add_argument(
            '--current',
            default=None,
            choices = [ "queued", "inprogress", "active", "deleted", "disabled" ],
            action = 'append',
            help='Only include runners whose current status matches this value (default is unspecified)',
        )
        parser.add_argument(
            '--desired',
            default=None,
            choices = [ "queued", "inprogress", "active", "deleted", "disabled" ],
            action = 'append',
            help='Only include runners whose desired status matches this value (default is unspecified)',
        )
        parser.add_argument(
            '--mismatch',
            action='store_true',
            default=False,
            help='Include runners in which the desired and current statuses are different.',
        )
        parser.add_argument(
            '--inactive',
            action='store_true',
            default=False,
            help='Include inactive runners as well as active ones (default is just active runners)',
        )
        parser.add_argument(
            '--resource',
            default=None,
            help='Restrict to just this type of resource (use title case, e.g. Login, Website, DomainZone)',
        )
        parser.add_argument(
            '--id',
            default=0,
            type=int,
            help='Restrict to just the runner with this id.',
        )
        parser.add_argument(
            '--member',
            default=0,
            type=int,
            help='Restrict to just runners associated with this member id.',
        )
        parser.add_argument(
            '--submit',
            default=False,
            action='store_true',
            help='Submit or re-submit all found runners (default is to just list them).',
        )
        parser.add_argument(
            '--data',
            default=False,
            action='store_true',
            help='Display full data.',
        )


    def get_status_constant(self, string):
        if 'queued' == string:
            return models.RunnerCommon.STATUS_PENDING_QUEUED
        elif 'inprogress' == string:
            return models.RunnerCommon.STATUS_PENDING_INPROGRESS
        elif 'active' == string:
            return models.RunnerCommon.STATUS_ACTIVE
        elif 'deleted' == string:
            return models.RunnerCommon.STATUS_DELETED
        elif 'disabled' == string:
            return models.RunnerCommon.STATUS_DISABLED


    def handle(self, *args, **options):
        rows = []
        for resource_class in models.get_all_member_resource_classes():
            if options['resource']:
                if options['resource'] != resource_class.__name__:
                    continue
            runner_object = resource_class.get_runner_object()
            if runner_object:
                queryset = runner_object.objects.filter()
                if options['mismatch']:
                    # Limit to records in which the current_status is different from the desired status.
                    queryset = queryset.filter(~Q(current_status = F('desired_status')))

                if options['current']:
                    statuses = []
                    for current in options['current']:
                        statuses.append(self.get_status_constant(current))
                    queryset = queryset.filter(current_status__in=statuses)

                if options['desired']:
                    statuses = []
                    for desired in options['desired']:
                        statuses.append(self.get_status_constant(desired))
                    queryset = queryset.filter(desired_status__in=statuses)

                if options['inactive'] == False:
                    queryset = queryset.filter(is_active=True)

                for runner in queryset:
                    if options['id']:
                        if runner.id != options['id']:
                            continue
                    if options['submit']:
                        self.update(runner)
                    else:
                        rows.append(self.get_row(runner, options['data']))

        if not options['submit']:
            headers = [ "Id", "Mid", "Class", "Active", "Desires", "Status", "Created", "Consumed", "Error" ]
            if options['data']:
                headers.append("Data")
            print(tabulate(rows, headers = headers))

    def get_row(self, runner=None, display_data = None):
        if runner.resource:
            member_id = runner.resource.id
        else:
            member_id = "[D]"
        if runner.is_active:
            active = 'act'
        else:
            active = 'inact'
        if runner.nonce_consumed:
            nonce_consumed = runner.nonce_consumed.strftime("%Y-%m-%d %H:%M")
        else:
            nonce_consumed = None

        row = [ runner.id, member_id, runner.__class__.__name__[:-6], active, runner.get_desired_status_display(), runner.get_current_status_display(), runner.nonce_created.strftime("%Y-%m-%d %H:%M"), nonce_consumed, runner.error ]

        if display_data:
            row.append(runner.data)

        return row

    def update(self, runner=None):
        runner.nonce = get_random_password(64)
        runner.nonce_created = timezone.now()
        runner.nonce_consumed = None
        runner.save()
        if runner.__class__.__name__[:-6] == 'DomainZone':
            force = True
            runner.submit(force)
        else:
            runner.submit()

