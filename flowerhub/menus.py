from django.urls import reverse_lazy
from django.utils.translation import gettext as _
import re

class FlowerhubMenus():
    # The full menu, with all options.
    menu = [ 
        { 
            'label': _("Logins"),
            'link': 'login-list',
            'icon': 'fa fa-user-circle',
            'match': re.compile("^(login|access)"),
            'secondary': [
                {
                    'label': _("All logins"),
                    'link': 'login-list',
                    'icon': 'fa fa-user-circle',
                    'match': re.compile("^login")
                },
                {
                    'label': _("Access"),
                    'link': 'access-list',
                    'icon': 'fa fa-key',
                    'match': re.compile("^access")
                },
            ]
        },
        {
            'label': _("Email"),
            'link': 'email-list',
            'icon': 'fa fa-envelope',
            'match': re.compile("^(email|mailing-list(/|$))"),
            'secondary': [
                {
                    'label': _("Email"),
                    'link': 'email-list',
                    'icon': 'fa fa-envelope-open',
                    'match': re.compile("^email")
                },
                {
                    'label': _("Mailing Lists"),
                    'link': 'mailing-list-list',
                    'icon': 'fa fa-mail-bulk',
                    'match': re.compile("^mailing")

                },
            ]
        },
        { 
            'label': _("Web sites"),
            'link': 'website-list',
            'icon': 'fa fa-spider',
            'match': re.compile("^(website|mysql)"),
            'secondary': [
                {
                    'label': _("Web sites"),
                    'link': 'website-list',
                    'icon': 'fa fa-spider',
                    'match': re.compile("^website")
                },
                {
                    'label': _("MySQL Databases"),
                    'link': 'mysql-database-list',
                    'icon': 'fa fa-database',
                    'match': re.compile("^mysql")
                },

            ]
        },
        { 
            'label': _("Domains"),
            'link': 'domain-zone-list',
            'icon': 'fa fa-compass',
            'match': re.compile("^(domain|mailing-list-domain)"),
            'secondary': [
                {
                    'label': _("Domain Zones"),
                    'link': 'domain-zone-list',
                    'icon': 'fa fa-compass',
                    'match': re.compile("^domain")
                },
                {
                    'label': _("Mailing List Domains"),
                    'link': 'mailing-list-domain-list',
                    'icon': 'fa fa-snowflake',
                    'match': re.compile("^mailing-list-domain")
                },
            ]
        },
        { 
            'label': _("Other"),
            'link': 'contact-list',
            'icon': 'fa fa-id-card',
            'match': re.compile("^contact"),
            'secondary': [
                {
                    'label': _("Contacts"),
                    'link': 'contact-list',
                    'icon': 'fa fa-id-card',
                    'match': re.compile("^contact")
                },
            ]
        },
    ] 


    def get_menu(self, current_view_name = None, member_pk = None):
        """
        Iterate over the entire menu and, based on the current view, add a flag
        to the approrpriate menu item that is currently active.

        Also, only include the secondary menu for the primary menu that is currently
        active.
        """

        # current_view_name will be something like /member/<int>/blah-list.
        # We want to strip off /member/<int> to leave just blah-list.
        current_view_name = re.sub('/member/[0-9]+/', '', current_view_name)

        # The variable holding the menu we will return to the template.
        active_menu = { 'primary': [], 'secondary': [] } 

        for item in self.menu:
            primary_active = ''
            if item['match'].match(current_view_name):
                # This primary menu item is active in the browser
                primary_active = 'active'

                # Since this is the active primary menu we should populate the secondary
                # menu with it's children 
                for child in item['secondary']:
                    secondary_active = '' 
                    if child['match'].match(current_view_name):
                        # We found the active secondary menu.
                        secondary_active = 'active' 
                    # Append this secondary menu item to our secondary menu.
                    active_menu['secondary'].append({'label': child['label'], 'link': child['link'], 'active': secondary_active, 'icon': child['icon']})

            # Add all primary menus
            active_menu['primary'].append({'label': item['label'], 'link': item['link'], 'icon': item['icon'], 'active': primary_active})

        return active_menu
