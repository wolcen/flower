""" 
These are shared between the hub and the petal apps.
"""

import sys

# We have a lot of variables assigned to values that are stored in the database
# and access by both the petal and the hub code. We keep all of those defined
# in this shared module so we can be sure we are referring to the same values.

PETAL_ACTION_NOOP = 1
PETAL_ACTION_CREATE_LDAP_LOGIN = 2
PETAL_ACTION_DELETE_LDAP_LOGIN = 3
PETAL_ACTION_DISABLE_LDAP_LOGIN = 4
PETAL_ACTION_ADD_EMAIL_ADDRESS = 5
PETAL_ACTION_ADD_EMAIL_DOMAIN = 6
PETAL_ACTION_REMOVE_EMAIL_ADDRESS = 7
PETAL_ACTION_REMOVE_LAST_EMAIL_ADDRESS = 8
PETAL_ACTION_CREATE_DOMAIN_ZONE = 9
PETAL_ACTION_DELETE_DOMAIN_ZONE = 10
PETAL_ACTION_REMOVE_EMAIL_DOMAIN = 11 
PETAL_ACTION_CREATE_MAILDIR = 12
PETAL_ACTION_DELETE_MAILDIR = 13
PETAL_ACTION_ADD_EMAIL_ALIAS = 14
PETAL_ACTION_REMOVE_EMAIL_ALIAS = 15
PETAL_ACTION_CREATE_WEBSTORE_SITE = 16
PETAL_ACTION_DISABLE_WEBSTORE_SITE = 17
PETAL_ACTION_DELETE_WEBSTORE_SITE = 18
PETAL_ACTION_CREATE_WEBCACHE_SITE = 19
PETAL_ACTION_DISABLE_WEBCACHE_SITE = 20
PETAL_ACTION_DELETE_WEBCACHE_SITE = 21


def log_error(*args, **kwargs):
    print(*args, **kwargs)
    sys.stdout.flush()

def log_info(*args, **kwargs):
    print(*args, **kwargs)
    sys.stdout.flush()

def log_debug(*args, **kwargs):
    print(*args, **kwargs)
    sys.stdout.flush()



